/**
 * \file cli.cc
 *
 * \brief Binary to control the server
 *
 * Simple tool to send updates to the server. Allows to either `add` and
 * `remove` addresses from the server.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <czmq.h>
#include <zsys.h>

//! Prints usage
void usage(const char *cmd) {
    fprintf(stderr, "Usage: %s [-h host] [-p port] add|remove addr1 addr2 ...\n", cmd);
}

//! Simply parses arguments and pushes everything to server
int main(int argc, char **argv) {
    int port = 7054;
    int opt;
    int verbose = 0;
    const char *srv = "127.0.0.1";
    char *cmd, *addr;

    // Check arguments
    while((opt = getopt(argc, argv, "p:h:v")) != -1) {
        switch (opt) {
            case 'p':
                port = atoi(optarg);
                break;
            case 'h':
                srv = optarg;
                break;
            case 'v':
                verbose = 1;
                break;
            default:
                zsys_error("Invalid option %s", optarg);
                usage(argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    // Check the rest of the command
    if(optind > argc - 2) {
        zsys_error("Required arguments are missing!\n");
        usage(argv[0]);
        exit(EXIT_FAILURE);
    }
    cmd = argv[optind++];
    if(strcmp(cmd, "add")!=0 && strcmp(cmd, "del") != 0) {
        zsys_error("Command has to be either 'add' or 'del'!");
        usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    // Send command
    zsock_t *sock = zsock_new(ZMQ_REQ);
    zsock_connect(sock, "tcp://%s:%d", srv, port);
    zmsg_t* msg = zmsg_new();
    if(verbose)
        zsys_info("Sending '%s' command with following addresses:", cmd);
    zmsg_addstr(msg, cmd);
    while(optind < argc) {
        if(strcmp(argv[optind], "-") == 0) {
            zsys_info("Reading from stdin");
            char buff[1024];
            int len;
            while((fgets(buff, 1024, stdin) == buff) && (len = strlen(buff) > 0)) {
                zsys_info("Got an element");
                if(buff[len-1] == '\n')
                    buff[len-1] = 0;
                if(verbose)
                    zsys_info(" * %s", buff);
                zmsg_addstr(msg, buff);
            }
        } else {
            if(verbose)
                zsys_info(" * %s", argv[optind]);
            zmsg_addstr(msg, argv[optind]);
        }
        optind++;
    }
    zmsg_send(&msg, sock);

    // Get reply
    msg = zmsg_recv(sock);
    char *ret = zmsg_popstr(msg);

    // Cleanup
    zmsg_destroy(&msg);
    zsock_destroy(&sock);

    // Exit
    if(ret != NULL && strcmp(ret, "OK") == 0) {
        zsys_info("Update sent.");
        free(ret);
        return 0;
    } else {
        zsys_info("Update failed.");
        if(ret)
            free(ret);
        return EXIT_FAILURE;
    }
}
