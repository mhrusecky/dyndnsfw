/**
 * \file client.h
 *
 * \brief Interface for client implemetnation
 *
 */

#pragma once

#include "bad_guy_manager.h"

/**
 * \brief Class that handles client side of the connection
 *
 */
class client {
    private:
        //! Authentication actor
        zactor_t *auth;
        //! Monitoring actor
        zactor_t *clientmon;
        //! Receiving socket
        zsock_t *sock;
    public:
        /**
         * \brief Constructor
         *
         * \param endpoint Where to connect to
         * \param certpath Path to the server public certificate
         */
        client(const char *endpoint, const char *certpath);
        //! Try to receive a message and process it via [bad_guy_manager class](@ref bad_guy_manager)
        int process_message(bad_guy_manager *bad);
        //! Destructor
        virtual ~client();
};

