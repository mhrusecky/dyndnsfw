/**
 *
 * \file bad_guy_manager.h
 *
 * \brief Abstract ancestor for bad guys collectors
 *
 * Simple abstraction over various blocking _engines_.
 */

#pragma once

//! Abstract ancestor for bad guys collectors
class bad_guy_manager {
    public:
        //! Add a bad guy to the database
        virtual int add(const char *addr);
        //! Remove a bad guy from the database
        virtual int del(const char *addr);
        //! Start fresh
        virtual int clear();
};

