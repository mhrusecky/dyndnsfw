/**
 * \file client-knot.cc
 *
 * \brief Binary to update policy module in Knot Resolver based on updates from server
 *
 */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <czmq.h>
#include <zsys.h>
#include <getopt.h>

#include "client.h"
#include "utils.h"
#include "knot_manager.h"

//! Verbosity level
int verbose = 0;

/**
 * \brief Main loop iterating over the messages
 *
 * Creates a [knot manager class](@ref knot_manager) and starts processing messages.
 *
 * \param endpoint Where to connect to
 * \param certpath Public server certificate
 * \param sock Path to the directory containing Knot Resolver controller socket
 */
void encrypted_loop(const char *endpoint, const char *certpath, const char* sock) {
    zsys_info("Connecting to %s using %s", endpoint, certpath);
    int ret = 0;

    knot_manager knot(sock);
    client cl(endpoint,certpath);

    //  Main loop
    while(zsys_interrupted == 0 && ret < 1) {
        ret = cl.process_message(&knot);
    }
}

//! Main body to parse command line arguments
int main(int argc, char **argv) {
    // Setup defaults
    const char *server = "sentinel.turris.cz";
    const char *cert = "/var/run/dynfw/server.cert";
    int port = 7053;
    const char *sock = "/var/run/kresd/control/";

    // Parse options
    struct option long_options[] = {
        {"server",     required_argument, 0,  's'},
        {"port",       required_argument, 0,  'p'},
        {"cert",       required_argument, 0,  'c'},
        {"socket-dir", required_argument, 0,  'k'},
        {"verbose",    no_argument,       0,  'v'},
        {"help",       no_argument,       0,  'h'},
        {0,0,0,0}
    };
    int c;
    int option_index = 0;
    while((c = getopt_long(argc, argv, "s:p:c:k:vh", long_options, &option_index)) != -1) {
        switch(c) {
            case 's':
                server = optarg;
                break;
            case 'c':
                cert = optarg;
                break;
            case 'p':
                port = atoi(optarg);
                break;
            case 'k':
                sock = optarg;
                break;
            case 'v':
                verbose = 1;
                break;
            case 'h':
                printf(
                    "Usage: %s [OPTION...]\n\n"
                    "   -s, --server=SERVER     server to connect to (default %s)\n"
                    "   -p, --port=PORT         port on the server to connect to (default %d)\n"
                    "   -c, --cert=CERT         path to the server certificate (default %s)\n"
                    "   -k, --socket-dir=DIR    directory with knot resolver control socket (default %s)\n"
                    "   -v, --verbose           turn on verbose output\n"
                    "   -h, --help              show help message and exit\n\n"
                    , argv[0], server, port, cert, sock
                );
                exit(0);
        };
    }

    char buff[1024];
    snprintf(buff, 1023, "tcp://%s:%d", server, port);
    zsys_init();
    if(!zsys_file_exists(cert)) {
        fprintf(stderr, "Server certificate %s not found, check your CLI arguments.", cert);
        exit(-1);
    }
    encrypted_loop(buff, cert, sock);
    return 0;
}
