/**
 * \file server.h
 *
 * \brief Implementation of [server class](@ref server)
 *
 */

#pragma once

#include <czmq.h>
#include <set>
#include <string>

/**
 * \brief Class to provide a simple server
 *
 * It establishes a publish socket to send updates of the list of bad guys. It
 * manages local cache of bad guys (loaded from and stored back to the disk).
 * Can send heartbeat and list messages on request and whenever list of bad
 * guys changes publishes a delta message. 
 *
 */
class server {
    private:
        //! List of bad guys to be blocked
        std::set<std::string> bad_guys;
        //! Authentication actor
        zactor_t *auth;
        //! Server monitoring actor
        zactor_t *servermon;
        /**
         * \brief Serial number of the currently sent list
         *
         * Increments with every change of the list of bad guys.
         * Serial is to inform client that it missed some messages.
         * We are trying to avoid duplicate serials within reasonable time frame.
         * Upper 16 bits of serial are lower 16 bits of current epoch.
         * Lower 16 bits are clear by default and incremented with changes.
         * Current design of serial guarantees conflict less behaviour if:
         *
         * * we do less then 2^16 updates within second
         * * we do updates and client receives some messages within 2^16
         */
        unsigned long long serial;
        //! Name of the on disk cache of bad guys.
        std::string cache_file;
        //! Outgoing socket to send data to clients
        zsock_t *sock;
    public:
        //! Send heartbeat and list messages
        void heartbeat();
        //! Adds an address to be blocked and sends delta message
        void add_bad_guy(const char* addr);
        //! Remove an address from blocked ones and sends delta message
        void remove_bad_guy(const char* addr);
        //! Server constructor
        server(const char *endpoint, const char *cert, const char* cache_file);
        //! Server destructor
        virtual ~server();
};
