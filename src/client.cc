#include <czmq.h>
#include <zsys.h>
#include <stdlib.h>
#include <string.h>

#include "client.h"
#include "utils.h"

client::client(const char* endpoint, const char* certpath) {
    //  Create and start authentication engine
    auth = zactor_new(zauth, NULL);
    if(verbose)
        zstr_sendx(auth,"VERBOSE", NULL);
    zsock_wait(auth);
    zstr_sendx(auth,"ALLOW", "0.0.0.0", NULL);
    zsock_wait(auth);
 
    //  Tell the authenticator how to handle CURVE requests
    zstr_sendx(auth,"CURVE", CURVE_ALLOW_ANY, NULL);
    zsock_wait(auth);

    //  We need two certificates, one for the client and one for
    //  the server. The client must know the server's public key
    //  to make a CURVE connection.
    zcert_t *client_cert = zcert_new ();
    zcert_t *server_cert = zcert_load(certpath);
    const char *server_key = zcert_public_txt(server_cert);
        
    //  Create and connect client socket
    sock = zsock_new(ZMQ_SUB);
    //  Use monitor to monitor the connection (and get debug output)
    clientmon = zactor_new(zmonitor, sock);
    if(verbose)
        zstr_sendx(clientmon, "VERBOSE", NULL);
    zstr_sendx(clientmon, "LISTEN", "ALL", NULL);
    zstr_sendx(clientmon, "START", NULL);
    zcert_apply(client_cert, sock);
    zsock_set_curve_serverkey(sock, server_key);
    zsock_set_subscribe(sock, "baddns/list");
    zsock_connect(sock, "%s", endpoint);
    zcert_destroy(&client_cert);
    zcert_destroy(&server_cert);
}

client::~client() {
    //  Cleanup
    zactor_destroy(&clientmon);
    zactor_destroy(&auth);
    zsock_destroy(&sock);
}

/**
 * \brief Helper function to change subscriptions
 *
 * Changes subscription set on socket from one topic to another.
 *
 * \param sock  Socket to work with
 * \param topic Complete topic we are currently subscribed to
 * \param from  What to strip
 * \param to    What to add
 */
void resubscribe(zsock_t *sock, const char *topic, const char* from, const char* to) {
    char *tmp = (char*)malloc(strlen(topic) - strlen(from) + strlen(to) + 1);
    strncpy(tmp, topic, strlen(topic) - strlen(from));
    tmp[strlen(topic) - strlen(from)] = 0;
    strcat(tmp, to);
    if(verbose)
        zsys_info("Changing subscription '%s' -> '%s' (%s -> %s)", topic, tmp, from, to);
    zsock_set_subscribe(sock, tmp);
    zsock_set_unsubscribe(sock, topic);
    free(tmp);
}

int client::process_message(bad_guy_manager* bad) {
    // We need to store serial to see when we missed messages
    static unsigned long long sserial = 0;
    unsigned long long serial = 0;

    // We also need to know what return code to return
    int ret = 0;

    // Get a message
    zmsg_t *msg = zmsg_recv(sock);
    if(msg == NULL)
        return -1;

    // Let's disassemble the message
    char *topic = NULL;
    char *cmd = NULL;
    char *tmp = NULL;
    if(topic = zmsg_popstr(msg))
        cmd = zmsg_popstr(msg);

    // Check that we got topic and content
    if(topic == NULL || cmd == NULL) {
        zsys_error("No topic or content");
        ret=-1;
        goto process_message_cleanup;
    }

    // Bootstrap
    if(strstr(topic,"/list") != NULL) {
        if(verbose)
            zsys_info("Received a list message - %s", topic);
        // Get and verify serial
        if(cmd == NULL) {
            zsys_error("Truncated list message received");
            ret=-1;
            goto process_message_cleanup;
        }
        errno = 0;
        sserial = strtoull(cmd, NULL, 10);
        if(errno == EINVAL) {
            zsys_error("Invalid serial number - '%s'", cmd);
            ret=-1;
            goto process_message_cleanup;
        }

        // Subscribe to updates only
        resubscribe(sock, topic, "list", "delta");

        // Process bad guys
        bad->clear();
        for(tmp = zmsg_popstr(msg); tmp != NULL; tmp = zmsg_popstr(msg)) {
            bad->add(tmp);
            free(tmp);
        }

        goto process_message_cleanup;
    }

    // Something weird, probably error
    if(strstr(topic,"/delta") == NULL) {
        zsys_error("Unsupported topic - '%s'", topic);
        ret=-1;
        goto process_message_cleanup;
    }

    // Only option left is delta message

    // filter out invalid messages
    if(verbose)
        zsys_info("Received a delta message - %s", topic);
    if((strncmp(cmd,"add", 3) != 0) && (strncmp(cmd,"del", 3) != 0)) {
        zsys_error("Unsupported delta message - msg '%s'", cmd);
        ret=-1;
        goto process_message_cleanup;
    }

    // If we are adding new rules, get serial
    if(strncmp(cmd,"add", 3) == 0 && strlen(cmd) > 5) {
        serial = strtoull(cmd+4, NULL, 10); // 'cmd = add[1234]' where 1234 is serial
    }

    // Removing is done via complete flush as removing is rare
    if(strncmp(cmd,"del", 3) == 0 && strlen(cmd) > 5) {
        serial = strtoull(cmd+4, NULL, 10); // 'cmd = del[1234]' where 1234 is serial
    }

    // Double check if serial makes sense
    if(serial == 0) {
        zsys_error("Unsupported delta message - msg '%s'", cmd);
        ret=-1;
        goto process_message_cleanup;
    }

    // Check for missing messages
    sserial++;
    if(serial != sserial) {
        zsys_error("Missed some delta messages (expected %llu, received %llu)", sserial, serial);
        resubscribe(sock, topic, "delta", "list");
        goto process_message_cleanup;
    }

    // Process new bad guys
    for(tmp = zmsg_popstr(msg); tmp != NULL; tmp = zmsg_popstr(msg)) {
        if((cmd[0] == 'a' && bad->add(tmp) != 0) || (cmd[0] == 'd' && bad->del(tmp) != 0)) {
            if((errno == ECONNRESET) || (errno == ESRCH)) {
                zsys_info("Reloading the list as there was a temporary error.");
                resubscribe(sock, topic, "delta", "list");
            } else {
                zsys_error("Error writing to the backend, giving up.");
                ret=1;
            }
            goto process_message_cleanup;
        }
        free(tmp);
    }

    // Destroy everything
process_message_cleanup:
    if(topic)
        free(topic);
    if(cmd)
        free(cmd);
    if(tmp)
        free(tmp);
    zmsg_destroy(&msg);
    return ret;
}

