/**
 * \file knot_manager.h
 *
 * \brief Implementation of blocker via Knot Resolver
 *
 */

#pragma once

#include "bad_guy_manager.h"
/**
 * \brief Bad guy manager using Knot Resolver
 *
 * Class that manages DAF
 * (https://knot-resolver.readthedocs.io/en/stable/modules-daf.html) in Knot
 * Resolver. Implements [bad guy manager](@ref bad_guy_manager) API.
 *
 */
class knot_manager : public bad_guy_manager {
    private:
        //! Directory with control socket
        char* sock;
        //! Last up to 1023 characters of reply from Knot Resolver
        char* sock_reply[1024];
        //! Send command to Knot Resolver
        int send_cmd(const char* cmd, ...);
    public:
        //! Block specified address
        virtual int add(const char *addr);
        //! Unblock specified address
        virtual int del(const char *addr);
        //! Unblock everything
        virtual int clear();
        /**
         * \brief Constructor of Knot Manager
         *
         * \param sock_path Path to the directory where control socket resides.
         */
        knot_manager(const char* sock_path);
        //! Knot manager destructor
        virtual ~knot_manager();
};

