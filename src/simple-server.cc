/**
 * \file simple-server.cc
 *
 * \brief Main binary to run server
 *
 * This wraps all the elements like [server class](@ref server) and provides a
 * server binary to actually start everything. It also implements a simple API
 * to communicate with the server.
 *
 */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <czmq.h>
#include <zsys.h>
#include <getopt.h>
#include <signal.h>

#include "server.h"
#include "utils.h"

//! Verbosity level
int verbose = 0;
//! How often send heartbeats and list messages
int timeout = 5;

//! Helper function to remove spaces at the end of string
void trim(char* str) {
    char *endp = str + strlen(str);
    while(((*endp == 0) || isspace(*endp)) && (endp != str)) {
        *endp = 0;
        endp--;
    }
}

//! Handles API calls and periodically publishes the list
void main_loop(server &sr, const char *api_endpoint) {
    zsys_notice("API for control available on %s", api_endpoint);

    // Create API endpoint
    zsock_t *api = zsock_new(ZMQ_REP);
    zactor_t *apimon = zactor_new(zmonitor, api);
    if(verbose)
        zstr_sendx(apimon, "VERBOSE", NULL);
    zstr_sendx(apimon, "LISTEN", "ALL", NULL);
    zstr_sendx(apimon, "START", NULL);
    zsock_bind(api, "%s", api_endpoint);

    // Create a poller
    zpoller_t *poller = zpoller_new(api, NULL);

    //  Main loop
    void *polled;
    while(zsys_interrupted == 0) {
        polled = zpoller_wait(poller, timeout * 1000);
        // No API call arrived, let's publish heartbeat
        if(polled == NULL) {
            sr.heartbeat();
        // Handle API calls
        } else {
            zmsg_t *msg = zmsg_recv(polled);
            // First frame is command, rest are address
            char *cmd = zmsg_popstr(msg);
            char *addr = NULL;
            if(cmd != NULL) {
                // Adding new address
                if(strcmp(cmd, "add") == 0) {
                    while(addr = zmsg_popstr(msg)) {
                        trim(addr);
                        sr.add_bad_guy(addr);
                        if(addr)
                            free(addr);
                    }
                // Removing adddress
                } else if(strcmp(cmd, "del") == 0) {
                    while(addr = zmsg_popstr(msg)) {
                        trim(addr);
                        sr.remove_bad_guy(addr);
                        if(addr)
                            free(addr);
                    }
                } else {
                    zsys_error("Invalid API call - %s", cmd);
                }
                zstr_sendx(polled, "OK", NULL);
            } else {
                zsys_error("Incomplete API call received");
            }
            if(cmd)
                free(cmd);
            if(addr)
                free(addr);
            zmsg_destroy(&msg);
        }
    }
    zactor_destroy(&apimon);
    zsock_destroy(&api);
}

//! Main to parse command line arguments and run server loop
int main(int argc, char **argv) {
    // Setup defaults
    const char *cert = "/var/run/dynfw/server.cert";
    const char *store = "cache.txt";
    int port = 7053;
    int api_port = 7054;

    // Parse options
    struct option long_options[] = {
        {"port",     required_argument, 0,  'p'},
        {"api-port", required_argument, 0,  'a'},
        {"cert",     required_argument, 0,  'c'},
        {"store",    required_argument, 0,  's'},
        {"timeout",  required_argument, 0,  't'},
        {"verbose",  no_argument,       0,  'v'},
        {"help",     no_argument,       0,  'h'},
        {0,0,0,0}
    };
    int c;
    int option_index = 0;
    while((c = getopt_long(argc, argv, "p:c:s:a:vh", long_options, &option_index)) != -1) {
        switch(c) {
            case 'c':
                cert = optarg;
                break;
            case 's':
                store = optarg;
                break;
            case 'p':
                port = atoi(optarg);
                break;
            case 'a':
                api_port = atoi(optarg);
                break;
            case 't':
                timeout = atoi(optarg);
                break;
            case 'v':
                verbose = true;
                break;
            case 'h':
                printf(
                    "Usage: %s [OPTION...]\n\n"
                    "   -p, --port=PORT         port on the server to connect to (default %d)\n"
                    "   -a, --api-port=PORT     port used to control the server (default %d)\n"
                    "   -c, --cert=CERT         path to the server certificate (default %s)\n"
                    "   -s, --store=CACHE       file to store entries between restarts (default %s)\n"
                    "   -t, --timeout=CACHE     how often send out list messages and heartbeats at worst (default %d)\n"
                    "   -v, --verbose           turn on verbose output\n"
                    "   -h, --help              show help message and exit\n\n"
                    , argv[0], api_port, port, cert, store, timeout
                );
                exit(0);
        };
    }

    // Prepare server and API endpoint
    zsys_init();
    if(!zsys_file_exists(cert)) {
        fprintf(stderr, "Server certificate %s not found, check your CLI arguments.", cert);
        exit(-1);
    }
    char endpoint[1024];
    char api_endpoint[1024];
    snprintf(endpoint, 1023, "tcp://*:%d", port);
    snprintf(api_endpoint, 1023, "tcp://127.0.0.1:%d", api_port);

    // Create a server
    server sr(endpoint, cert, store);

    // Run main loop
    main_loop(sr, api_endpoint);
    return 0;
}
