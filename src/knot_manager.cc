#include <stdarg.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <errno.h>
#include <czmq.h>

#include "knot_manager.h"
#include "utils.h"

knot_manager::knot_manager(const char *socket) {
    asprintf(&sock, "%s", socket);
    clear();
}

knot_manager::~knot_manager() {
    clear();
    free(sock);
}

int knot_manager::send_cmd(const char* cmd, ...) {
    va_list ap;
    static char last_path[108];
    int ret = 0;
    char *buff = NULL;
    char socket_path[108]; // 108 is the maximum path length to UNIX socket

    // Get socket filename
    DIR *d = opendir(sock);
    if(d == NULL) {
        zsys_error("Socket directory not found - %s", sock);
        errno = ENOENT;
        return -1;
    }

    struct dirent *entry = NULL;
    struct stat check_file;
    while((entry = readdir(d)) != NULL) {
        snprintf(socket_path, 108, "%s/%s", sock, entry->d_name);
        if(stat(socket_path, &check_file) != 0)
            continue;
        if((check_file.st_mode & S_IFMT) == S_IFSOCK)
            break;
    }

    if(entry == NULL) {
        zsys_error("No socket found in %s", sock);
        errno = ESRCH;
        return -1;
    }
    if(snprintf(socket_path, 108, "%s/%s", sock, entry->d_name) >= 108) {
        zsys_error("Path to the socket is too long (%s/%s)", sock, entry->d_name);
        errno = E2BIG;
        return -1;
    }
    closedir(d);
    if(strcmp(last_path, socket_path) != 0) {
        strncpy(last_path, socket_path, 108);
        if(last_path[0] != 0) {
            errno = ECONNRESET;
            return 1;
        }
    }

    // Connect to the socket
    struct sockaddr_un addr;
    int data_socket;

    data_socket = socket(AF_UNIX, SOCK_STREAM, 0);
    assert(data_socket != -1);
    memset(&addr, 0, sizeof(struct sockaddr_un));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path) - 1);
    if(connect(data_socket, (const struct sockaddr *)&addr, sizeof(struct sockaddr_un)) == -1) {
        zsys_error("Can't connect to knot using socket %s", socket_path);
        errno = EIO;
        return -1;
    }
    
    // Construct command to send
    va_start(ap, cmd);
    vasprintf(&buff, cmd, ap);
    assert(buff != NULL);
    va_end(ap);

    // Send command
    write(data_socket, buff, strlen(buff));
    write(data_socket, "\n", 1);
    while(read(data_socket, sock_reply, sizeof(sock_reply)) == sizeof(sock_reply));
    if(verbose) {
        zsys_info("Command '%s' has been sent to knot", buff);
    }

    // Cleanup everything
    close(data_socket);
    free(buff);

    if(ret > 0) {
        errno = ECONNRESET;
    } else {
        errno = 0;
    }
    return ret;
}

int knot_manager::clear() {
    bad_guy_manager::clear();
    // Clear already present policies
    int ret = send_cmd(
            "if dyndnsfw ~= nil then"
            "    for _, data in pairs(dyndnsfw) do"
            "        if data['id'] ~= nil then"
            "            policy.del(data['id'])"
            "        end"
            "    end"
            "end");
    send_cmd("dyndnsfw = {}");
    return ret;
}

int knot_manager::add(const char *addr) {
    bad_guy_manager::add(addr);
    return send_cmd(
        "dyndnsfw['%s'] = policy.add("
        "    policy.suffix("
        "        policy.DENY_MSG("
        "            'Blocked by DNS firewall',"
        "            policy.todnames({'%s'})"
        "        )"
        "    )"
        ")", addr, addr);
}

int knot_manager::del(const char *addr) {
    bad_guy_manager::del(addr);
    int ret = send_cmd("policy.del(dyndnsfw['%s']['id'])", addr);
    send_cmd("dyndnsfw['%s']['id'] = nil", addr);
    return ret;
}
