#include <czmq.h>
#include <zsys.h>

#include "bad_guy_manager.h"
#include "utils.h"

int bad_guy_manager::clear() {
    if(verbose)
        zsys_info("Clearing list of bad guys");
    return 0;
}

int bad_guy_manager::add(const char *addr) {
    if(verbose)
        zsys_info("Adding bad guy %s", addr);
    return 0;
}

int bad_guy_manager::del(const char *addr) {
    if(verbose)
        zsys_info("Removing bad guy %s", addr);
    return 0;
}
