#include "server.h"
#include "utils.h"
#include <fstream>

void server::heartbeat() {
    if(verbose)
        zsys_info("Sending heartbeat (version %llu)", serial);
    zstr_sendx(sock, "heartbeat/beat", "beat", NULL);
    zmsg_t *msg = zmsg_new();
    zmsg_addstr(msg, "baddns/list");
    zmsg_addstrf(msg, "%llu", serial);
    for(auto addr:bad_guys) {
        zmsg_addstr(msg, addr.c_str());
    }
    zmsg_send(&msg, sock);
}

void server::add_bad_guy(const char* addr) {
    if(bad_guys.find(addr) != bad_guys.end()) {
        if(verbose)
            zsys_info("Address %s is already banned", addr);
        return;
    }
    serial = (serial + 1) & ( (1ull << 32) - 1);
    if(verbose)
        zsys_info("Sending update adding %s (serial %llu)", addr, serial);
    bad_guys.insert(addr);
    zmsg_t *msg = zmsg_new();
    zmsg_addstr(msg, "baddns/delta");
    zmsg_addstrf(msg, "add[%llu]", serial);
    zmsg_addstr(msg, addr);
    zmsg_send(&msg, sock);
}

void server::remove_bad_guy(const char* addr) {
    auto it = bad_guys.find(addr);
    if(it == bad_guys.end()) {
        if(verbose)
            zsys_info("Address %s is not banned", addr);
        return;
    }
    serial = (serial + 1) & ( (1ull << 32) - 1);
    if(verbose)
        zsys_info("Sending update removing %s (serial %llu)", addr, serial);
    bad_guys.erase(it);
    zmsg_t *msg = zmsg_new();
    zmsg_addstr(msg, "baddns/delta");
    zmsg_addstrf(msg, "del[%llu]", serial);
    zmsg_addstr(msg, addr);
    zmsg_send(&msg, sock);
}

/**
 * Construct the server that is accepting connections and is ready to send
 * updates.
 *
 * \param endpoint Endpoint to listen on
 * \param certpath Path to the file with certificate
 * \param cache_file Path to the file with offline cache
 */

server::server(const char *endpoint, const char *certpath, const char* cache_file_i) {
    // Initialize serial, see documentation in the header file
    serial = time(NULL);
    serial = (serial & ( (1ul << 16) - 1) ) << 16;

    // Load cache
    cache_file = cache_file_i;
    std::ifstream cache(cache_file);
    std::string addr;
    if(verbose)
        zsys_info("Loading cache from %s", cache_file_i);
    if(cache.is_open()) {
        while(getline(cache, addr)) {
            if(verbose)
                zsys_info("Adding %s", addr.c_str());
            bad_guys.insert(addr);
        }
    }

    // Create and start authentication engine
    auth = zactor_new(zauth, NULL);
    if(verbose)
        zstr_sendx(auth,"VERBOSE", NULL);
    zsock_wait(auth);
 
    // Tell the authenticator how to handle CURVE requests
    zstr_sendx(auth,"CURVE", CURVE_ALLOW_ANY, NULL);
    zsock_wait(auth);
    zcert_t *server_cert = zcert_load(certpath);
        
    // Create server socket
    sock = zsock_new(ZMQ_PUB);
    assert(sock != NULL);
    // Use monitor to monitor the connection (and get debug output)
    servermon = zactor_new(zmonitor, sock);
    if(verbose)
        zstr_sendx(servermon, "VERBOSE", NULL);
    zstr_sendx(servermon, "LISTEN", "ALL", NULL);
    zstr_sendx(servermon, "START", NULL);
    zsock_set_curve_server(sock, 1);
    zcert_apply(server_cert, sock);
    if(verbose)
        zsys_info("Publishing updates on %s", endpoint);
    zsock_bind(sock, "%s", endpoint);
    zcert_destroy(&server_cert);
}

server::~server() {
    //  Cleanup
    zactor_destroy(&servermon);
    zactor_destroy(&auth);
    zsock_destroy(&sock);

    // Save cache
    std::ofstream cache(cache_file);
    std::string addr;
    if(verbose)
        zsys_info("Saving cache to %s", cache_file.c_str());
    if(cache.is_open()) {
        for(auto addr : bad_guys) {
            cache << addr << '\n';
        }
    }

}

