The goal of this project is to have a simple tool to distribute list of
problematic DNS names to block. It is made of several components.

## Server

Binary `dyndnsfw-server` provides a server to store a list of addresses to be
blocked. On shutdown, it stores a list of addresses in text file that it loads
again on next start. Needs a certificate so clients can validate that it is
trust worthy server. To get a server certificate, use `zmakecert` from CZMQ
project.

Basic implementation is done in src/simple-server.cc file, which mainly wraps
the [server class](@ref server).

### Server control

Simple binary `dyndnsfw-server-ctl` allows you to add and remove entries from
the cache of problematic DNS addresses. Nothing more, nothing less. Implemented
in src/cli.cc file.

## Knot client

Binary `dyndnsfw-knot-client` connects to the server on one side and Knot
Resolver on the other side and uses Knot Resolvers policy module to block the domains
send from the server. It requires Knot Resolver version 6.0.

Implementation wise it wraps [client](@ref client) and
[knot_manager](@ref knot_manager) and the interface is implemented in
src/client-knot.cc.

## Protocol

Protocol to distribute updated list of problematic DNS names is based on ZeroMQ
PUB/SUB pattern.

There are three topics to subscribe to and each of them contain slightly
different messages.

### heartbeat/beat

Sends simple messages containing one frame with text content `beat`. Those are
send within specified time interval by server (can be send more often than
specified, but not less often). Can be used by client to detect whether
connection works.

### baddns/list

Meant to be used by client for bootstrapping the list when starting. Contains
multiple frames. First frame is serial number of the list represented as text
in decadic format. Should always fit into 32bit unsigned integer.

The rest of the frames contains one address per frame to be blocked.

| 1233        |
|-------------|
| example.org |
| example.cz  |
| ...         |

### baddns/delta

Meant to be used by client to keep the list up to date. Only changes are
published and always one at the time. Serial number is increased by one with
every change.

Every delta message contains exactly two frames. First frame is the command and
the second frame is the address. Command is either `add[num]` or `del[num]`,
where num is decadic representation of the serial.

| add[1234]   |
|-------------|
| example.com |

| del[1235]   |
|-------------|
| example.com |
